# LIS4381 - Mobile Web Application Development

## Brennan O'Hara

### Assignment 4 Requirements:

*Four Parts*

1. Develop Web Application that displays course work
2. Uses client-side validations and a working carousel (Bootstrap)
3. Chapter Questions
4. SkillSet 10-12


#### README.md file should include the following items:

* Screenshots of Course Work Web Application
    * Shows A4 client-side validation
* Screenshots of Skillsets 10-12
* Link to Web App: [Web Application](http://localhost/repos/lis4381/index.php)

### Assignment Screenshots:

#### My Course Work Web Application:

![GIF of A4 Web App](https://media.giphy.com/media/CAeKUO2fWyzLa7hYtw/giphy.gif)

#### GIF of Skillsets 7, 8, 9:
![GIF of A4 Skillsets](https://media.giphy.com/media/KXaOGg9Nw21hffTAt6/giphy.gif)