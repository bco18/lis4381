# LIS4381 - Mobile Web Application Development

## Brennan O'Hara

### Project 2 Requirements:

*Four Parts*

1. Display Server Validation
2. Demonstrate use of Edit with Failed Validation
3. Delete prompt and Successfully deleted record
4. RSS Feed


#### README.md file should include the following items:

* GIF of Web Application Running
* GIF of Server Side Form Validation
* GIF Edit and Delete Functions
* Screenshot of RSS Feed
* Link to Web App: [Web Application](http://localhost/repos/lis4381/index.php)

### Assignment Screenshots:

#### GIF of LIS 4381 Course Work Webpage:
![GIF of LIS4381 Webpage](https://media.giphy.com/media/DVFauZdDU8BA9PSW0y/giphy.gif)

