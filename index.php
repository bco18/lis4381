<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="LIS 4381: Mobile Web Application Development">
		<meta name="author" content="Brennan O'Hara">
		<link rel="icon" href="favicon.ico">

		<title>Career Portfolio</title>

		<?php include_once("css/include_css.php"); ?>	

		<!-- Carousel styles -->
		<style type="text/css">
		 h1
		 {
			 margin: 0;     
			 color: #a10330;
			 padding-top: 50px;
			 font-size: 48px;
			 font-family: "trebuchet ms", sans-serif; 
			 text-shadow: 3px 3px #bdbdbd
		 }
		 h2
		 {
			 margin: 0;     
			 color: #ffffff;
			 padding-top: 50px;
			 font-size: 52px;
			 font-family: "trebuchet ms", sans-serif;    
		 }
		 .item
		 {
			 background: #333;    
			 text-align: center;
			 height: 300px !important;
		 }
		 .carousel
		 {
			 margin: 20px 0px 20px 0px;
		 }
		 .bs-example
		 {
			 margin: 20px;
		 }
		</style>

	</head>
	<body>

		<?php include_once("global/nav_global.php"); ?>
		
		<div class="container">
			<div class="starter-template">
				<div class="page-header">
					<?php include_once("global/header.php"); ?>	
				</div>

				<!-- Start Bootstrap Carousel  -->
				<div class="bs-example">
					<div
						id="myCarousel"
								class="carousel"
								data-interval="2500"
								data-pause="hover"
								data-wrap="true"
								data-keyboard="true"			
								data-ride="carousel">
						
    				<!-- Carousel indicators -->
						<ol class="carousel-indicators">
							<li data-target="#myCarousel" data-slide-to="0" class="active"></li>
							<li data-target="#myCarousel" data-slide-to="1"></li>
							<li data-target="#myCarousel" data-slide-to="2"></li>
						</ol>   
						<!-- Carousel items -->
						<div class="carousel-inner">

							<!-- -Note: you will need to modify the code to make it work with *both* text and images.  -->
							<div class="active item" style="background: url(img/cyber.png) no-repeat left center; background-size: cover;" alt="LinkedIn">
								<a href="https://www.linkedin.com/in/bcohara">
								<div class="container">
									<h2>Brennan O'Hara's LinkedIn</h2>
									<div class="carousel-caption">
										<p class="lead">Keep up with me on LinkedIn</p>
									</div>
								</div>
								</a>
							</div>
							
							<div class="item" style="background: url(img/python.png) no-repeat left center; background-size: cover;" alt="Extensible Enterprise Solutions Link">
								<a href="https://bitbucket.org/bco18/lis4369">
								<h2>Extensible Enterprise Solutions</h2>
								<div class="carousel-caption">
									<p>Uses Python, R, and my SQL</p>
								</div>
								</a>
							</div>

							<div class="item" style="background: url(img/webapp.png) no-repeat left center; background-size: cover;" alt="Mobile Web Application Link">
								<a href="https://bitbucket.org/bco18/lis4381">
								<h2>Mobile Web Application Development</h2>
								<div class="carousel-caption">
									<p>Uses XML, Java, CSS, and PHP</p>							
								</div>
								</a>
							</div>

						</div>
						<!-- Carousel nav -->
						<a class="carousel-control left" href="#myCarousel" data-slide="prev">
							<span class="glyphicon glyphicon-chevron-left"></span>
						</a>
						<a class="carousel-control right" href="#myCarousel" data-slide="next">
							<span class="glyphicon glyphicon-chevron-right"></span>
						</a>
					</div>
				</div>
				<!-- End Bootstrap Carousel  -->
				
				<?php
				include_once "global/footer.php";
				?>

			</div> <!-- end starter-template -->
    </div> <!-- end container -->

		<?php include_once("js/include_js.php"); ?>	
	  
  </body>
</html>
