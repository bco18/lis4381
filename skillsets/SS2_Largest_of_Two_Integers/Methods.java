import java.util.Scanner;

public class Methods
{
    public static void getRequirements()
    {
        System.out.println("Developer: Brennan O'Hara");
        System.out.println("Program evaluates largest of two integers.");
        System.out.println("Note: Program does *not* check for non-numeric characters or non-integer values.");

        System.out.println();
    }

    public static void evaluateNumbers()
    {
        //initialize variables, create Scanner object, capture user input
        int x = 0;
        int y = 0;
        Scanner sc = new Scanner(System.in);

        System.out.print("Enter First Integer: ");
        x = sc.nextInt();

        System.out.print("Enter Second Integer: ");
        y = sc.nextInt();  
        
        if (x > y)
            {
                System.out.println();
                System.out.println(x +" is larger than " + y);
            }
        else if (x < y)
            {   
                System.out.println();
                System.out.println(y +" is larger than " + x);

            }
        else
            {
                System.out.println();
                System.out.println("Integers are equal");
            }


            }


    }


