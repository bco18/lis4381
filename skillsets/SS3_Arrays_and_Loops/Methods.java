public class Methods
{
    public static final String[] MY_ANIMALS = {"dog", "cat", "bird", "fish", "insect"};

    public static void getRequirements()
    {
        System.out.println("Developer: Brennan O'Hara");
        System.out.println("Program loops through array of strings.");
        System.out.println("Use following values: dog, cat, bird, fish, insect.");
        System.out.println("Use following loop structures: for, enhanced for, while, do...while.\n");
        System.out.println("Note: Pretest loops: for, enhanced for, while. Posttest loop: do...while.");
        System.out.println();
    }

    public static void showLoops()  {
        System.out.println("For Loops:");
            for (int i = 0; i < MY_ANIMALS.length; i++) {
            System.out.println(MY_ANIMALS[i]);


    }
        System.out.println("\nEnhanced for Loop:");
            for (String animals : MY_ANIMALS) {
            System.out.println(animals);
        
    }
        System.out.println("\nWhile Loop:");
        int d = 0;
            while (d < MY_ANIMALS.length) {
            System.out.println(MY_ANIMALS[d]);
            d++;
    }
        
        System.out.println("\nDo...While Loop:");
        int f = 0;
            do{
                System.out.println(MY_ANIMALS[f++]);
            }while(f < MY_ANIMALS.length);
        }    
        
}     