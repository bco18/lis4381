import java.util.Scanner;

public class Methods
{
    public static void getRequirements()
    {
        System.out.println("Developer: Brennan O'Hara");
        System.out.println("Program prompts user for first name and age, then prints results.");
        System.out.println("Create four methods from the following requirements");
        System.out.println("1) getRequirments(): Void method displays program requirements");
        System.out.println("2) getUserInput(): Void method prompts for user input,\n\tthen calls two methods: myVoidMethod() and my ValueReturningMethod().");
        System.out.println("3) myVoidMethod():\n\ta.Accepts two arguments: String and int.\n\tb. Prints user's first name and age.");
        System.out.println("4) myValueReturningMethod():\n\ta. Accepts two argumentsL String and int. \n\tb. Returns String containing first name and age.");

        System.out.println();
    }

    public static void getUserInput()
    {
        //initialize variables, create Scanner object, capture user input
        System.out.print("Enter First name: ");
        Scanner sc = new Scanner(System.in);
        String myName = sc.nextLine();
        System.out.print("Enter age: ");
        int myAge = sc.nextInt();

        myVoidMethod(myName, myAge);
        System.out.println("\nvalue-returning method call: " + myValueReturningMethod(myName, myAge));
    }

    public static void myVoidMethod(String myName, int myAge)
    {
        System.out.print("\nvoid method call: " + myName + " is " + myAge);
    }


    public static String myValueReturningMethod(String myName, int myAge)
    {
      String output = myName + " is " + myAge;
      return(output);
    }
}
