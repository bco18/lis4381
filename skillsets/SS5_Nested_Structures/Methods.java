import java.util.Scanner;

public class Methods
{
    public static void getRequirements()
    {
        System.out.println("Developer: Brennan O'Hara");
        System.out.println("Program searches user-entered integer w/in array of integers.");
        System.out.println("Create an array with the following values: 3, 2, 4, 99, -1, -5, 3, 7");
        System.out.println();
    }

    public static void evaluateSearch()
    {
      //initialized variables and arry
        int mySearch = 0;
        int i = 0;
        int[] myNum = {3, 2, 4, 99, -1, -5, 3, 7};
      // get user input
        System.out.println("Array length: " + myNum.length);
        Scanner sc = new Scanner(System.in);
        System.out.print("Enter search value: ");
        mySearch = sc.nextInt();
        System.out.println();
      // use nested structures to loop through index
        for(i = 0; i < myNum.length; i++)
        {
          if (myNum[i] == mySearch)
          {
            System.out.println(mySearch + " found at index " + i );
          }
          else
          {
            System.out.println(mySearch + " *not* found at index " + i );
          }
        }
    }
}
