# LIS4381 - Mobile Web Application Development

## Brennan O'Hara

### Project 1 Requirements:

*Four Parts*

1. Create My Business Card Application
2. Include an image, background color, and logo
3. Chapter Questions
4. SkillSet 7-9


#### README.md file should include the following items:

* Screenshots of My Business Card Application
    * Show 1st and 2nd Activity
* Screenshots of Skillsets 7-9

### Assignment Screenshots:

**My Business Card Application:**


|                                               |                                                                                         |                                               |
| ----------------------------------------------|-----------------------------------------------------------------------------------------|-----------------------------------------------|
|![Filler](img/download.png)                    |![GIF of My Business Card App](https://media.giphy.com/media/p1vUb4Yge4OArw1QBx/giphy.gif)|![Filler](img/download.png)                    |



#### GIF of Skillsets 7, 8, 9:
![GIF of P1 Skillsets](https://media.giphy.com/media/mqBqJUB2D6rbMVIVM5/giphy.gif)