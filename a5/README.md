# LIS4381 - Mobile Web Application Development

## Brennan O'Hara

### Assignment 5 Requirements:

*Four Parts*

1. Develop Web Page that utilizes MySQL to show and add Data
2. Utilize Server Side Validationa in Web Page
3. Chapter Questions
4. SkillSet 13-15


#### README.md file should include the following items:

* Screenshots of Web Application Running
* Screenshot of Server Side Form Validation
* Screenshots of Skillsets 13-15
* Link to Web App: [Web Application](http://localhost/repos/lis4381/index.php)

### Assignment Screenshots:

#### GIF of Pet Stores Web Page:
![GIF of Pet Stores Page](https://media.giphy.com/media/8FO6fg0V8l05AfVtCO/giphy.gif)

#### GIF of Skillsets 13,14,15:
![GIF of A5 Skillsets](https://media.giphy.com/media/bNHT68pUEYjqgsPjky/giphy.gif)