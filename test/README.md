> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381 - Mobile Web Application Development

## Brennan O'Hara

### Assignment 2 Requirements:

*Three Parts*

1. Healthy Recipes Mobile App
2. Chapter Questions (Chs 3, 4)
3. Bitbucket repo link


#### README.md file should include the following items:

* Screenshots of running mobile app
* Screenshots of Java SkillSets


#### Assignment Screenshots:

*GIF of Healthy Recipes Mobile App:*

|                                               |                                                                                   |                                               |
| ----------------------------------------------|-----------------------------------------------------------------------------------|-----------------------------------------------|
|![Filler](img/download.png)                    |![GIF of Recipes App](https://media.giphy.com/media/PlFxKFcOLuwzlsWAoe/source.gif) |![Filler](img/download.png)                    |



*Screenshots of Java Skill Sets:*

|Skill Set 1:Even Or Odd            |Skill Set 2:Largest of Two Integers |Skill Set 3: Arrays and Loops  |
|-----------------------------------|------------------------------------|-------------------------------|
|![SS1 Screenshot](img/SS1.PNG)     |![SS2 Screenshot](img/SS2.png)      |![SS3 Screenshot](img/SS3.png) |

