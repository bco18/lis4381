> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381 - Mobile Web Application Development

## Brennan O'Hara

### Assignment 3 Requirements:

*Four Parts*

1. Pet Store ERD
2. Concert Ticket Application
3. 10 Records in each pet store table
4. Links to .mwb and .sql file


#### README.md file should include the following items:

* Screenshots of Concert Tickets App Running
* Screenshots of Pet Store ERD
* Screenshot of 10 Records per table
* Link to A3 MWB: [a3_Concert_Tickets.mwb](docs/a3.mwb "A3 MWB")
* Link to A3 SQL: [a3_Concert_Tickets.sql](docs/a3.sql "A3 SQL")

### Assignment Screenshots:

**Concert Ticket Application:**


|                                               |                                                                                         |                                               |
| ----------------------------------------------|-----------------------------------------------------------------------------------------|-----------------------------------------------|
|![Filler](img/download.png)                    |![GIF of Concert Tickets App](https://media.giphy.com/media/DSejrSEIA68TNx6Ue2/giphy.gif)|![Filler](img/download.png)                    |


#### Screenshot of Pet Store ERD:
![Screeshot of ERD](img/a3_ERD.PNG) 

#### GIF of 10 Records from Pet Store Tables:
![GIF of Pet Store Tables](https://media.giphy.com/media/icsTtLcWO4v1juIY5D/giphy.gif)

#### GIF of Skillsets 4, 5, 5.2, and 6:
![GIF of A3 Skillsets](https://media.giphy.com/media/l56fXhltd9wrOb7izU/giphy.gif)